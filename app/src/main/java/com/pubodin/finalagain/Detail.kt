package com.pubodin.finalagain

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.pubodin.finalagain.databinding.FragmentDetailBinding
import com.pubodin.finalagain.model.BooksItem
import com.pubodin.finalagain.model.Datasource
import com.squareup.picasso.Picasso

class Detail : Fragment() {

    private var _binding: FragmentDetailBinding? = null
    private val binding get() = _binding
    private var result: Int = 0
    private var dataList : BooksItem? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        result = arguments?.getInt("id")!!
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        getData()
        (activity as AppCompatActivity).supportActionBar!!.title = dataList?.titleName

        _binding = FragmentDetailBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Picasso.get()
            .load(dataList?.image)
            .into(binding?.bookImage)
        binding?.titleTextView?.text  = dataList?.titleName
        binding?.detailTextView?.text = "Detail : "+dataList?.des
        binding?.typeText?.text =  "Author : "+dataList?.author +"   Type : "+dataList?.type
        binding?.priceText?.text = "Price : "+dataList?.price+"฿"
        nowId = dataList?.id!!
        if(dataList?.fav == 1){
            binding?.btnAddFav?.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_favorite_white,0,0,0)
            binding?.btnAddFav?.setOnClickListener {
                Toast.makeText(requireContext(),"Remove from Favorite",Toast.LENGTH_LONG).show()
                binding?.btnAddFav?.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_favorite_border_white,0,0,0)
                for (i in Datasource.getData){
                    if(i.id == result){
                        Datasource.getData[i.id-1].fav = 0
                        reload()
                        break
                    }
                }


            }
        }else if(dataList?.fav == 0){
            binding?.btnAddFav?.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_favorite_border_white,0,0,0)
            binding?.btnAddFav?.setOnClickListener {
                Toast.makeText(requireContext(),"Added to Favorite",Toast.LENGTH_LONG).show()
                binding?.btnAddFav?.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_favorite_white,0,0,0)
                for (i in Datasource.getData){
                    if(i.id == result){
                        Datasource.getData[i.id-1].fav = 1
                        reload()
                        break
                    }
                }
            }
        }

        binding?.btnSearch?.setOnClickListener {
            val queryUrl: Uri = Uri.parse(dataList?.url)
            val intent = Intent(Intent.ACTION_VIEW,queryUrl)
            requireContext().startActivities(arrayOf(intent))
        }
        super.onViewCreated(view, savedInstanceState)

        binding?.homeFloatingActionButton?.setOnClickListener {
            val action = DetailDirections.actionDetailFragmentToHomeFragment()
            findNavController().navigate(action)
        }


    }


    private fun reload(){
            val action = DetailDirections.actionDetailFragmentSelf(nowId)
            findNavController().navigate(action)
    }

    private fun getData() {
        for (i in Datasource.getData){
            if(i.id == result){
                dataList = i
                break
            }
        }
    }


}
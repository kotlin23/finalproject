package com.pubodin.finalagain

import android.app.Application
import com.pubodin.finalagain.database.BooksDatabase

class BookApplication : Application() {
    val database: BooksDatabase by lazy { BooksDatabase.getDatabase(this) }
}
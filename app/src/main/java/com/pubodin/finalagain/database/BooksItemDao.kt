package com.pubodin.finalagain.database

import androidx.room.*
import com.pubodin.finalagain.model.BooksItem
import kotlinx.coroutines.flow.Flow

@Dao
interface BooksItemDao {
    @Insert
    suspend fun insert(item: BooksItem)

    @Update
    suspend fun update(item: BooksItem)

    @Delete
    suspend fun delete(item: BooksItem)

    @Query("SELECT * FROM BooksItem WHERE id = :id")
    fun getItem(id: Int): Flow<BooksItem>

    @Query("SELECT * FROM BooksItem ORDER BY titleName ASC")
    fun getItems(): Flow<List<BooksItem>>

    @Query("SELECT * FROM BooksItem WHERE fav = 1")
    fun getFav(): Flow<List<BooksItem>>
}
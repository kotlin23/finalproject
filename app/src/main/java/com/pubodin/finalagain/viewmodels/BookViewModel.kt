package com.pubodin.finalagain.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.pubodin.finalagain.model.BooksItem
import com.pubodin.finalagain.database.BooksItemDao
import kotlinx.coroutines.flow.Flow


class BookViewModel(private val bookDao:BooksItemDao): ViewModel(){
    fun allBooks(): Flow<List<BooksItem>> = bookDao.getItems()
    fun book(id:Int): Flow<BooksItem> = bookDao.getItem(id)
}

class BookViewModelFactory(private val bookDao: BooksItemDao) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(BookViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return BookViewModel(bookDao) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
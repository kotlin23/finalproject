package com.pubodin.finalagain

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.pubodin.finalagain.databinding.FragmentHomeBinding
import com.pubodin.finalagain.model.BooksItem
import com.pubodin.finalagain.model.Datasource
import com.pubodin.finalagain.viewmodels.BookViewModel
import com.pubodin.finalagain.viewmodels.BookViewModelFactory
import com.squareup.picasso.Picasso

var nowId : Int = 0
class Home : Fragment() {
    private var dataList: List<BooksItem> = listOf()
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    private val viewModel: BookViewModel by activityViewModels {
        BookViewModelFactory(
            (activity?.application as BookApplication).database.bookDao()
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as AppCompatActivity).supportActionBar!!.title = "Books suggestions"
        _binding = FragmentHomeBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //getMyDataApi()
        getData()
        val recyclerView = binding.recyclerView
        recyclerView.adapter = ItemAdapter(dataList,requireContext()){
            val action = HomeDirections.actionHome2ToDetail(it.id)
            nowId = it.id
            findNavController().navigate(action)
        }
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding.favoriteFloatingActionButton.setOnClickListener {
            val action = HomeDirections.actionHomeFragmentToFavoriteFragment()
            findNavController().navigate(action)
        }

    }


    private fun getData(){
        dataList = Datasource.getData
//        viewModel.allBooks().collect(){
//            data = it as ArrayList<BooksItem>
//        }
//        dataList = data

//        lifecycle.coroutineScope.launch{
//            viewModel.allBooks().collect(){
//                dataList = it
//            }
//        }

    }

    class ItemAdapter(private val dataList: List<BooksItem>, private val context: Context, private val click : (BooksItem)->Unit) : RecyclerView.Adapter<ItemAdapter.ViewHolder>() {
//        companion object {
//            private val DiffCallback = object : DiffUtil.ItemCallback<BooksItem>() {
//                override fun areItemsTheSame(oldItem: BooksItem, newItem: BooksItem): Boolean {
//                    return oldItem.id == newItem.id
//                }
//
//                override fun areContentsTheSame(oldItem: BooksItem, newItem: BooksItem): Boolean {
//                    return oldItem == newItem
//                }
//            }
//        }


        class ViewHolder(private val view : View) : RecyclerView.ViewHolder(view){
            val titleName: TextView = view.findViewById(R.id.bookName)
            val price: TextView = view.findViewById(R.id.price)
            val picture : ImageView = view.findViewById(R.id.picture)
            val type : TextView = view.findViewById(R.id.type)


        }
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val adapterLayout = LayoutInflater.from(parent.context).inflate(R.layout.item_view, parent,false)
            return ViewHolder(adapterLayout)
        }


        override fun getItemCount(): Int {
            return dataList.size
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val data = dataList[position]
            val titleName = holder.titleName
            val price = holder.price
            val picture = holder.picture
            val type = holder.type
            titleName.text = data.titleName
            price.text = data.price.toString()+"฿"
            type.text = data.type
            Picasso.get()
                .load(data.image)
                .into(picture)
            holder.itemView.setOnClickListener {
                click(data)
            }
        }


    }



}
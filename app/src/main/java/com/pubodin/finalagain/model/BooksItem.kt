package com.pubodin.finalagain.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class BooksItem(
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    @ColumnInfo(name = "titleName")
    var titleName: String,
    @ColumnInfo(name = "author")
    var author: String,
    @ColumnInfo(name = "type")
    var type: String,
    @ColumnInfo(name = "des")
    var des: String,
    @ColumnInfo(name = "price")
    var price: Int,
    @ColumnInfo(name = "image")
    var image: String,
    @ColumnInfo(name = "url")
    var url: String,
    @ColumnInfo(name = "fav")
    var fav: Int
)
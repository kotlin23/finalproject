package com.pubodin.finalagain.model

object Datasource {
    var getData =
         mutableListOf(
            BooksItem(
                1,
                "BURNING HELL นครแห่งพระเจ้า (เล่มเดียวจบ)",
                "YANG KYUNG-IL","Cartoon",
                "ในประเทศที่ไม่เหลือซึ่งความดีความชั่ว เพราะสงครามและความอดอยากที่ดำเนินมาอย่างยาวนาน มือสังหารได้จัดการฆ่าทหารองครักษ์ส่วนองค์ชาย อี มุน ที่รอดชีวิตมาได้เพียงคนเดียว ก็ใช้เงินจ้างโจรภูเขานามว่าจูฮาแล้วหลบหนีไปด้วยกัน นี่คือเรื่องราวการผจญภัย แฟนตาซีขององค์ชายกับโจรภูเขา!!",
                185,
                "https://inwfile.com/s-du/indlip.jpg",
                "https://www.google.co.th/search?q=BURNING+HELL+%E0%B8%99%E0%B8%84%E0%B8%A3%E0%B9%81%E0%B8%AB%E0%B9%88%E0%B8%87%E0%B8%9E%E0%B8%A3%E0%B8%B0%E0%B9%80%E0%B8%88%E0%B9%89%E0%B8%B2+%28%E0%B9%80%E0%B8%A5%E0%B9%88%E0%B8%A1%E0%B9%80%E0%B8%94%E0%B8%B5%E0%B8%A2%E0%B8%A7%E0%B8%88%E0%B8%9A%29",
                0
            ),
            BooksItem(
                2,
                "World Ghost",
                "Yim, Kang-jae",
                "Cartoon",
                "พบกับเรื่องเล่าสุดหลอนจากโรงเรียนทั่วโลก ตำนานภูคผีปีศาจและสิ่งลึกลับที่เล่าขานกันมาจากรุ่นสู่รุ่น",
                175,
                "https://storage.naiin.com/system/application/bookstore/resource/product/202201/541131/1000246217_front_XL.jpg?t=au&imgname=%E0%B8%9C%E0%B8%B5%E0%B8%97%E0%B8%B1%E0%B9%88%E0%B8%A7%E0%B9%82%E0%B8%A5%E0%B8%81-%E0%B9%80%E0%B8%A3%E0%B8%B7%E0%B9%88%E0%B8%AD%E0%B8%87%E0%B8%9C%E0%B8%B5%E0%B9%86-%E0%B8%A3%E0%B8%AD%E0%B8%9A%E0%B9%82%E0%B8%A5%E0%B8%81",
                "https://www.google.co.th/search?q=World+Ghost+Yim%2C+Kang-jae",
                0
            ),
            BooksItem(
                3,
                "DRAGON BALL SUPER",
                "Akira Toriyama",
                "Cartoon",
                "การต่อสู้ระหว่างกองทหารของโมโรที่มายังโลกกับเหล่านักสู้ดาวโลกได้เริ่มปะทุขึ้น! ในที่สุดโมโรก็ลงมายังพื้นโลก พวกโงฮังต้องสู้กับซากัมโบ้ที่โมโรเพิ่มพลังให้อย่างยากลำบาก โงคูกับเบจิต้าจะมาช่วยโลกที่กำลังวิกฤติได้ทันหรือไม่!?",
                175,
                "https://cdn-shop.ookbee.com/Books/NEDCLUB_CODE/2020/20200811071554/Thumbnails/Cover.jpg",
                "https://www.google.co.th/search?q=DRAGON+BALL+SUPER+Akira+Toriyama",
                0
            ),
            BooksItem(4,
                "MY HERO ACADEMIA",
                "Kohei Horikoshi",
                "Cartoon",
                "นอกจากจะปล่อยให้ชิงาราคิหนีไปได้ ความเสียหายยังหนักหนานักแต่ว่า...ถึงอย่างนั้นพวกเราฮีโร่ก็ยังมีปณิธานแน่วแน่เป็นหนึ่งเดียวว่าจะต้องต่อสู้ต่อไปให้ได้!",
                70,"https://pbs.twimg.com/media/EbbbFmIUwAESYCA?format=jpg&name=4096x4096",
                "https://www.google.co.th/search?q=MY+HERO+ACADEMIA+Kohei+Horikoshi",
                0
            ),
            BooksItem(
                5,
                "WAKE UP WITH THE KISS",
                "NANA HARUTA","Cartoon",
                "ใครจะไปชอบน้องชายแก่แดดแบบนั้น...กันล่ะ การพบพานนี้คือโชคชะตา? หรือจะเป็นเรื่องต้องห้ามกันแน่...!?เรื่องราวความรักของพี่น้องบุญธรรมได้เริ่มต้นขึ้นแล้วเอนะผู้มีน้องชาย น้องสาว บุญธรรมเพิ่มขึ้นมาจากการแต่งงานใหม่ของแม่ แต่ โทมะ น้องชายบุญธรรมที่เป็นพื่อนร่วมชั้นกลับพูดอย่างไร้เยื่อใยว่า ห้ามชอบฉันเด็ดขาดเลยนะ?",
                95,
                "https://inwfile.com/s-du/wgmcc1.png",
                "https://www.google.co.th/search?q=WAKE+UP+WITH+THE+KISS+NANA+HARUTA",
                0
            ),
            BooksItem(
                6,
                "SHADOWS HOUSE",
                "Somato",
                "Cartoon",
                "เมื่อ การสังเกตการณ์ ถูกจัดขึ้นพวกเด็กๆ ก็อลหม่านวุ่นวายสิ่งที่เริ่มปรากฏชัดขึ้นเรื่อยๆคือสิ่งที่เก็บเอาไว้ไม่อยู่จากการขาด กาแฟ ระหว่างที่สูญเสียระเบียบควบคุมไปและการล้างสมองค่อยๆ บางเบาลงนั้นสิ่งที่แวบไปแวบมาก็คือความทรงจำในอดีตคำตอบหนึ่งที่หลับใหลอยู่ ณ ที่แห่งนั้นคือ...",
                160,
                "https://cf.shopee.co.th/file/222c791a6c43a8ce3a0608718a4a7a5d",
                "https://www.google.co.th/search?q=SHADOWS+HOUSE+Somato",
                0
            ),
            BooksItem(
                7,
                "มิเอรุ ใครว่าหนูเห็นผีโกะจัง",
                "อิสึมิ โทโมกิ",
                "Cartoon",
                "มังงะ \\'สยองขำ\\' ของเด็กสาวผู้โชคดี!? ที่จะทำให้คุณฮาจนขนหัวลุก!",
                160,
                "https://storage.naiin.com/system/application/bookstore/resource/product/202111/536589/1000244790_front_XXL.jpg?imgname=%E0%B8%A1%E0%B8%B4%E0%B9%80%E0%B8%AD%E0%B8%A3%E0%B8%B8-%E0%B9%83%E0%B8%84%E0%B8%A3%E0%B8%A7%E0%B9%88%E0%B8%B2%E0%B8%AB%E0%B8%99%E0%B8%B9%E0%B9%80%E0%B8%AB%E0%B9%87%E0%B8%99%E0%B8%9C%E0%B8%B5%E0%B9%82%E0%B8%81%E0%B8%B0%E0%B8%88%E0%B8%B1%E0%B8%87-%E0%B9%80%E0%B8%A5%E0%B9%88%E0%B8%A1-2-(Mg)",
                "https://www.google.co.th/search?q=%E0%B8%A1%E0%B8%B4%E0%B9%80%E0%B8%AD%E0%B8%A3%E0%B8%B8+%E0%B9%83%E0%B8%84%E0%B8%A3%E0%B8%A7%E0%B9%88%E0%B8%B2%E0%B8%AB%E0%B8%99%E0%B8%B9%E0%B9%80%E0%B8%AB%E0%B9%87%E0%B8%99%E0%B8%9C%E0%B8%B5%E0%B9%82%E0%B8%81%E0%B8%B0%E0%B8%88%E0%B8%B1%E0%B8%87+%E0%B8%AD%E0%B8%B4%E0%B8%AA%E0%B8%B6%E0%B8%A1%E0%B8%B4+%E0%B9%82%E0%B8%97%E0%B9%82%E0%B8%A1%E0%B8%81%E0%B8%B4",
                1
            ),
             BooksItem(
                 8,
                 "สิ่งมหัศจรรย์ที่เกิดขึ้นเมื่อฉันลองตื่นก่อนโลก",
                 "คิมยูจิน",
                 "Psychology",
                 "ใช้ชั่วโมงอันสงบยามเช้ามืดที่มีอานุภาพลึกลับไม่เหมือนเวลาใด ฟื้นคืนพลังในการควบคุมชีวิตและพลิกโชคชะตาของคุณ",
                 225,
                 "https://storage.naiin.com/system/application/bookstore/resource/product/202111/536542/1000244758_front_XXL.jpg?imgname=%E0%B8%AA%E0%B8%B4%E0%B9%88%E0%B8%87%E0%B8%A1%E0%B8%AB%E0%B8%B1%E0%B8%A8%E0%B8%88%E0%B8%A3%E0%B8%A3%E0%B8%A2%E0%B9%8C%E0%B8%97%E0%B8%B5%E0%B9%88%E0%B9%80%E0%B8%81%E0%B8%B4%E0%B8%94%E0%B8%82%E0%B8%B6%E0%B9%89%E0%B8%99%E0%B9%80%E0%B8%A1%E0%B8%B7%E0%B9%88%E0%B8%AD%E0%B8%89%E0%B8%B1%E0%B8%99%E0%B8%A5%E0%B8%AD%E0%B8%87%E0%B8%95%E0%B8%B7%E0%B9%88%E0%B8%99%E0%B8%81%E0%B9%88%E0%B8%AD%E0%B8%99%E0%B9%82%E0%B8%A5%E0%B8%81",
                 "https://www.google.co.th/search?q=%E0%B8%AA%E0%B8%B4%E0%B9%88%E0%B8%87%E0%B8%A1%E0%B8%AB%E0%B8%B1%E0%B8%A8%E0%B8%88%E0%B8%A3%E0%B8%A3%E0%B8%A2%E0%B9%8C%E0%B8%97%E0%B8%B5%E0%B9%88%E0%B9%80%E0%B8%81%E0%B8%B4%E0%B8%94%E0%B8%82%E0%B8%B6%E0%B9%89%E0%B8%99%E0%B9%80%E0%B8%A1%E0%B8%B7%E0%B9%88%E0%B8%AD%E0%B8%89%E0%B8%B1%E0%B8%99%E0%B8%A5%E0%B8%AD%E0%B8%87%E0%B8%95%E0%B8%B7%E0%B9%88%E0%B8%99%E0%B8%81%E0%B9%88%E0%B8%AD%E0%B8%99%E0%B9%82%E0%B8%A5%E0%B8%81",
                 0
             ),
             BooksItem(
                 9,
                 "ดาบพิฆาตอสูร แฟนบุ๊ค บันทึกกลุ่มพิฆาตอสูร",
                 "Koyoharu Gotouge",
                 "Cartoon",
                 "ดาบพิฆาตอสูร แฟนบุ๊ค บันทึกกลุ่มพิฆาตอสูร เล่ม 2",
                 210,
                 "https://cf.shopee.co.th/file/beac3682e82ea521024a49d85008804e_tn",
                 "https://www.google.co.th/search?q=%E0%B8%94%E0%B8%B2%E0%B8%9A%E0%B8%9E%E0%B8%B4%E0%B8%86%E0%B8%B2%E0%B8%95%E0%B8%AD%E0%B8%AA%E0%B8%B9%E0%B8%A3%20%E0%B9%81%E0%B8%9F%E0%B8%99%E0%B8%9A%E0%B8%B8%E0%B9%8A%E0%B8%84%20%E0%B8%9A%E0%B8%B1%E0%B8%99%E0%B8%97%E0%B8%B6%E0%B8%81%E0%B8%81%E0%B8%A5%E0%B8%B8%E0%B9%88%E0%B8%A1%E0%B8%9E%E0%B8%B4%E0%B8%86%E0%B8%B2%E0%B8%95%E0%B8%AD%E0%B8%AA%E0%B8%B9%E0%B8%A3",
                 0
             ),
             BooksItem(
                 10,
                 "CHAIN SAW MAN เล่ม 1 (ฉบับการ์ตูน)",
                 "Tatsuki Fujimoto",
                 "Cartoon",
                 "เด็นจิเด็กหนุ่มที่จนกรอบยิ่งกว่ากรอบโดนจิกหัวใช้ ในฐานะนักล่าปิศาจร่วมกับปิศาจโปจิตะ ชีวิตในแต่ละวันที่ย่ำแย่ต่ำเตี้ยติดดินเปลี่ยนไปในพริบตาจากการทรยศที่แสนโหดร้าย!! เรื่องแอ็คชั่นของฮีโร่สายดาร์คสมัยใหม่ ผู้ให้ปิศาจสิงสู่ร่าง ล่าล้างปิศาจ เปิดฉาก!",
                 57,
                 "https://storage.naiin.com/system/application/bookstore/resource/product/201908/484753/1000222976_front_XXL.jpg?imgname=CHAIN-SAW-MAN-%E0%B9%80%E0%B8%A5%E0%B9%88%E0%B8%A1-1-(%E0%B8%89%E0%B8%9A%E0%B8%B1%E0%B8%9A%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B9%8C%E0%B8%95%E0%B8%B9%E0%B8%99)",
                 "https://www.google.com/search?q=CHAIN+SAW+MAN+%E0%B9%80%E0%B8%A5%E0%B9%88%E0%B8%A1+1+(%E0%B8%89%E0%B8%9A%E0%B8%B1%E0%B8%9A%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B9%8C%E0%B8%95%E0%B8%B9%E0%B8%99)",
                 0
             )

        )


}